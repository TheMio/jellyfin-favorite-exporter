# Jellyfin favorite exporter / backup tool

Export all your favorites from your jellyfin library and transcode them into smaller files while keeping best possible quality. Useful when wanting to have your favorites with you on a phone, but without filling up all of it's space with big flacs.

# Setup / Install

1. Clone this repository
2. Perform a `yarn install`
3. Insert your data into the `config.json`
4. Run the script with `yarn start`
5. All the compressed files will be stored in the configured path, or by default in `output/opus`