// https://stackoverflow.com/questions/37213316/execute-batch-of-promises-in-series-once-promise-all-is-done-go-to-the-next-bat
export async function promiseAllInBatches(tasks: Array<() => Promise<any>>, batchSize = 3) {
    let position = 0;
    let results = [];
    while (position < tasks.length) {
        const itemsForBatch = tasks.slice(position, position + batchSize);
        results = [...results, ...await Promise.allSettled(itemsForBatch.map(item => item()))];
        position += batchSize;
    }
    return results;
}