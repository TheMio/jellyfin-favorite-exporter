import axios from 'axios';
import contentDispositionParser from 'content-disposition-parser';
import { createWriteStream, existsSync } from 'fs';
import { mkdir } from 'fs/promises';
import path from 'path';
import { finished } from 'stream/promises';
import {
    apiToken,
    favoriteSourceUserUuid,
    outputDir,
    outputFormats,
    url
} from './config.json';
import { execPromise } from './lib/execPromise';
import { promiseAllInBatches } from './lib/promiseAllInBatches';
import sanitize from 'sanitize-filename';


interface JellyfinMediaItem { Name: string, Id: string, Album: string, AlbumArtist: string, IndexNumber?: number, Type: 'Audio' | string }
interface JellyfinMediaItemsResponse {
    Items: JellyfinMediaItem[], TotalRecordCount: number
}
interface SourceFileInfo {
    relativePath: string, albumFolder: string
}

async function main() {
    // Global stats
    const resultStats = {
        downloadSuccess: new Array<string>(),
        downloadFailed: new Array<string>(),
        encodeSuccess: new Array<string>(),
        encodeFailed: new Array<string>()
    };

    // Get all favorites over api and download files
    const rawFiles: SourceFileInfo[] = [];
    const httpHeaders = { 'Authorization': `MediaBrowser Token="${apiToken}"` };
    try {
        console.log("Fetching user favorites");
        const res = await axios.get<JellyfinMediaItemsResponse>(`${url}/Users/${favoriteSourceUserUuid}/Items?includeItemTypes=Audio,MusicAlbum,MusicArtist&isFavorite=true&enableTotalRecordCount=true&recursive=true`,
            { headers: httpHeaders })
        console.log(`Found ${res.data.TotalRecordCount} records`);

        const processMediaItemDownload = async (mediaItem: JellyfinMediaItem) => {
            try {
                console.log(`Downloading ${mediaItem.Album} - ${mediaItem.Name}`);
                const albumFolder = path.join(sanitize(mediaItem.AlbumArtist || 'unknown'), sanitize(mediaItem.Album || 'unknown'));
                const dirName = path.join(outputDir, 'raw', albumFolder);
                await mkdir(dirName, { recursive: true });
                const newFile = await downloadFile(`${url}/Items/${mediaItem.Id}/Download`, httpHeaders, dirName, true);
                rawFiles.push({ relativePath: newFile, albumFolder });
                resultStats.downloadSuccess.push(newFile);
            } catch (e) {
                resultStats.downloadFailed.push(mediaItem.Name);
                console.log(e);
            }
        }

        const downloadPromises = res.data.Items.filter(i => i.Type === 'Audio').map(i => () => processMediaItemDownload(i));
        await promiseAllInBatches(downloadPromises, 8)

    } catch (e) {
        console.debug(e)
    }

    // Encode all media files to defined output formats
    for (const outputFormat of outputFormats) {
        const encodeMedia = async (fileInfo: SourceFileInfo) => {
            try {
                const dirName = path.join(outputDir, newFileExtension + ' ' + outputFormat.bitrate, fileInfo.albumFolder);
                await mkdir(dirName, { recursive: true });
                const transcodedFileName = path.join(dirName, sanitize(path.parse(fileInfo.relativePath).name) + "." + newFileExtension);
                if (!existsSync(transcodedFileName)) {
                    console.log("Encoding " + transcodedFileName)
                    await execPromise(`ffmpeg -i "${fileInfo.relativePath}" -vn -c:a ${outputFormat.encoder} -y -b:a ${outputFormat.bitrate} "${transcodedFileName}"`);
                    resultStats.encodeSuccess.push(transcodedFileName);
                }
            } catch (e) {
                console.log(e);
                resultStats.encodeFailed.push(fileInfo.relativePath);
            }
        }

        console.log(`Start encoding into ${outputFormat.container}/${outputFormat.encoder}`);
        const newFileExtension = outputFormat.container;

        const encodingPromises = rawFiles.map(f => () => encodeMedia(f));
        await promiseAllInBatches(encodingPromises, 8);
    }

    // Todo: Cleanup unused files

    // FinishchunkPromise
    console.log("Finished! Here the stats:");
    console.log(Object.keys(resultStats).map(k => resultStats[k].length));
}



async function downloadFile(fileUrl: string, httpHeaders: any, outputDirectoryPath: string, skipExisting: true) {
    return axios({
        method: 'get',
        url: fileUrl,
        responseType: 'stream',
        headers: httpHeaders
    })
        .then(async response => {
            const contentDispositionTags = contentDispositionParser(response.headers["content-disposition"]);
            if (!contentDispositionTags.filename) throw new Error("No filename found for download");
            const outputFilename = path.join(outputDirectoryPath, decodeURIComponent(contentDispositionTags.filename));
            if (skipExisting && !existsSync(outputFilename)) {
                const writer = createWriteStream(outputFilename);
                response.data.pipe(writer);
                await finished(writer); //this is a Promise
            }
            return outputFilename
        });
}

main();